# Devine qui est de retour ? Le devin !

Pour cette version en javascript modulaire, nous trouvons les fichiers suivants:

- `devin.mjs` : le code principal, sous forme de module javascript.
- `main.mjs` : le programme principal, qui utilise le module précédent
- `index.html` : la page à charger pour exécuter le code.

Dans l'entête du html, pour charger les 2 modules, vous trouvez:
```html
  <head>
    <script type="module" src="devin.mjs"></script>
    <script type="module" src="main.mjs"></script>
  </head>

```

- `test/devin_test.mjs` : le programme de test
- `test.html` : la page à charger pour exécuter les tests.

La sécurité des modules dans l'environnement du navigateur requiert l'utilisation d'URL en http ou https.

Il faut donc lancer un serveur, vous pouvez par exemple utiliser:

```shell
python -mSimpleHTTPServer
```

puis ouvrir [http://localhost:8000/](http://localhost:8000/index.html)
