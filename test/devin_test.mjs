import {handleGuess} from '../devin.mjs'
console.log(`devin_test is loaded`)

function test(real,expected){
  const real2s = JSON.stringify(real)
  const expected2s = JSON.stringify(expected)
  console.log(`${real2s} should be equal to ${expected2s}`)
}

test(handleGuess(10,20,2), {found: false, message: 'too low'})
test(handleGuess(10,0,2), {found: false, message: 'too big'})
test(handleGuess(20,20,2), {found: false, message: 'found in 2'})

