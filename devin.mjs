'use strict'
console.log('devin.js loaded')

// target <- generate random number
// loop until found
// ask to guess
// result <- compare guess to target, +1 try
// display message(result)

// generate random int between 1 and max, both included
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max)) + 1
}


export function handleGuess(guess, target, tries){
  let message
  if (guess == target)
    message = `found in ${tries}`
  if (guess < target)
    message = 'too low'
  if (guess > target)
    message = 'too big'
  return {
    found: guess == target,
    message: message
  }
}

export default function devin(){
  let found = false
  const target = getRandomInt(50)
  let guess
  let tries = 1
  console.log(`target is ${target}`)

  while (!found){
    let result
    tries++
    guess = prompt('make a guess : ')
    console.log(`tries = ${tries}`)
    result = handleGuess(guess, target, tries)
    console.log(result.message)
    found = result.found
  }
}
